using System.Collections.Generic;

namespace Kk.Csx.Utils.Graph
{
    public readonly struct Edge<T>
    {
        public readonly T From;
        public readonly T To;

        public Edge(T from, T to)
        {
            From = from;
            To = to;
        }

        public override string ToString()
        {
            return "{" + From + "=>" + To + "}";
        }

        private sealed class FromToEqualityComparer : IEqualityComparer<Edge<T>>
        {
            public bool Equals(Edge<T> x, Edge<T> y)
            {
                return EqualityComparer<T>.Default.Equals(x.From, y.From) &&
                       EqualityComparer<T>.Default.Equals(x.To, y.To);
            }

            public int GetHashCode(Edge<T> obj)
            {
                unchecked
                {
                    return (EqualityComparer<T>.Default.GetHashCode(obj.From) * 397) ^
                           EqualityComparer<T>.Default.GetHashCode(obj.To);
                }
            }
        }

        public static IEqualityComparer<Edge<T>> Comparer { get; } = new FromToEqualityComparer();
    }
}