using System;
using System.Collections.Generic;
using System.Linq;
using Leopotam;

namespace Kk.Csx.Utils
{
    public static class LangUtils
    {
        public static IEnumerable<T> OrEmpty<T>(this IEnumerable<T> list)
        {
            return list ?? EmptyListHolder<T>.EmptyList;
        }

        public static T NotNull<T>(this T obj)
        {
            if (obj == null)
            {
                throw new Exception("is null");
            }

            return obj;
        }

        public static float ClampUp(this float x, float lowerBound)
        {
            return Math.Max(x, lowerBound);
        }

        public static float ClampDown(this float x, float upperBound)
        {
            return Math.Min(x, upperBound);
        }

        private static class EmptyListHolder<T>
        {
            public static readonly List<T> EmptyList = new List<T>();
        }

        public static TV GetOrDefault<TK, TV>(this IDictionary<TK, TV> dict, TK key)
        {
            if (dict.TryGetValue(key, out var value))
            {
                return value;
            }

            return default;
        }

        public static string ShallowListToString<T>(this IEnumerable<T> list, string delimiter = ", ")
        {
            return list
                .Select(x => x.ToString())
                .Aggregate((a, b) => a + delimiter + b);
        }
    }
}